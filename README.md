Irudia sortzeko
===============

Garapenean zuzenean exekutatuta DNS erroreak ematen zizkidan. Nire ordenagailuan muntatutakoan exekutatu behar izan dut (Itur).

sudo docker build -t iametza/collective-access:bertsio-zenbakia .

bertsio-zenbakia ordez jarri dagokiona: 1.7.8.(azkena + 1)

Azken bertsioa zein den jakiteko: https://hub.docker.com/r/iametza/collective-access/tags

Irudia hub-era igotzeko
=======================

Baliteke aurretik Hub-ean saioa hasi behar izatea terminalean.

sudo docker image push iametza/collective-access:bertsio-zenbakia
